
/* c016.c: **********************************************************}
{* Téma:  Tabulka s Rozptýlenými Položkami
**                      První implementace: Petr Přikryl, prosinec 1994
**                      Do jazyka C prepsal a upravil: Vaclav Topinka, 2005
**                      Úpravy: Karel Masařík, říjen 2014
**                      Úpravy: Radek Hranický, říjen 2014
**                      Úpravy: Radek Hranický, listopad 2015
**
** Vytvořete abstraktní datový typ
** TRP (Tabulka s Rozptýlenými Položkami = Hash table)
** s explicitně řetězenými synonymy. Tabulka je implementována polem
** lineárních seznamů synonym.
**
** Implementujte následující procedury a funkce.
**
**  HTInit ....... inicializuje tabulku před prvním použitím
**  HTInsert ..... vložení prvku
**  HTSearch ..... zjištění přítomnosti prvku v tabulce
**  HTDelete ..... zrušení prvku
**  HTRead ....... přečtení hodnoty prvku
**  HTClearAll ... zrušení obsahu celé tabulky (inicializace tabulky
**                 poté, co již byla použita)
**
** Definici typů naleznete v souboru c016.h.
**
** Tabulka je reprezentována datovou strukturou typu tHTable,
** která se skládá z ukazatelů na položky, jež obsahují složky
** klíče 'key', obsahu 'data' (pro jednoduchost typu float), a
** ukazatele na další synonymum 'ptrnext'. Při implementaci funkcí
** uvažujte maximální rozměr pole HTSIZE.
**
** U všech procedur využívejte rozptylovou funkci hashCode.  Povšimněte si
** způsobu předávání parametrů a zamyslete se nad tím, zda je možné parametry
** předávat jiným způsobem (hodnotou/odkazem) a v případě, že jsou obě
** možnosti funkčně přípustné, jaké jsou výhody či nevýhody toho či onoho
** způsobu.
**
** V příkladech jsou použity položky, kde klíčem je řetězec, ke kterému
** je přidán obsah - reálné číslo.
*/

#include "c016.h"
#include <stdio.h>

int HTSIZE = MAX_HTSIZE;
int solved;

/*          -------
** Rozptylovací funkce - jejím úkolem je zpracovat zadaný klíč a přidělit
** mu index v rozmezí 0..HTSize-1.  V ideálním případě by mělo dojít
** k rovnoměrnému rozptýlení těchto klíčů po celé tabulce.  V rámci
** pokusů se můžete zamyslet nad kvalitou této funkce.  (Funkce nebyla
** volena s ohledem na maximální kvalitu výsledku). }
*/

int hashCode ( tKey key ) {
	int retval = 1;
	int keylen = strlen(key);
	for ( int i=0; i<keylen; i++ )
		retval += key[i];
	return ( retval % HTSIZE );
}

/*
** Inicializace tabulky s explicitně zřetězenými synonymy.  Tato procedura
** se volá pouze před prvním použitím tabulky.
*/

void htInit ( tHTable* ptrht ) {
	int i = HTSIZE;
	if (ptrht == NULL){					// Testování zda ukazatel je NULL
		return;
	} else {
		while (i){
			(*ptrht)[i - 1] = NULL;		// Nastavuji kazdy ukazatel na NULL
			i--;
		}
	}
}

/* TRP s explicitně zřetězenými synonymy.
** Vyhledání prvku v TRP ptrht podle zadaného klíče key.  Pokud je
** daný prvek nalezen, vrací se ukazatel na daný prvek. Pokud prvek nalezen není,
** vrací se hodnota NULL.
**
*/

tHTItem* htSearch ( tHTable* ptrht, tKey key ) {
	tHTItem *pom = NULL;
	if ((*ptrht) == NULL){				// Testování zda ukazatel je NULL
		return (NULL);
	} else {
		pom = (*ptrht)[hashCode(key)];	// Ziskám ukazatel na další položku
		while (pom != NULL){
			if (pom->key == key){		// Testovaání pokud jsem našel hledanou položku
				return (pom);
			} else {
				pom = pom->ptrnext;
			}
		}
	}
	return (NULL);
}

/*
** TRP s explicitně zřetězenými synonymy.
** Tato procedura vkládá do tabulky ptrht položku s klíčem key a s daty
** data.  Protože jde o vyhledávací tabulku, nemůže být prvek se stejným
** klíčem uložen v tabulce více než jedenkrát.  Pokud se vkládá prvek,
** jehož klíč se již v tabulce nachází, aktualizujte jeho datovou část.
**
** Využijte dříve vytvořenou funkci htSearch.  Při vkládání nového
** prvku do seznamu synonym použijte co nejefektivnější způsob,
** tedy proveďte.vložení prvku na začátek seznamu.
**/

void htInsert ( tHTable* ptrht, tKey key, tData data ) {
	if ((*ptrht) == NULL){										// Test zda ukazatel je NULL
		return;
	} else {													// Není NULL
		tHTItem *pom = htSearch(ptrht, key);					// Vyhledání v tabulce podle klíče
		if (pom == NULL){										// Test zda nenašel
			tHTItem *new = (tHTItem *) malloc(sizeof(tHTItem));	// Alokace paměti pro novou položku
			if (new == NULL){									// Test zda alokakce neuspěla
				return;
			} else {											// Alokace uspěla a naplníme strukturu
				new->data = data;
				new->key = key;
				int i = hashCode(key);							// Získání hashovacího klíče
				new->ptrnext = (*ptrht)[i];						// Provázání se seznamem
				(*ptrht)[i] = new;

			}
		} else {							// Prvek byl nalezen
			pom->data = data;				// Aktualizace dat
		}
	}
}

/*
** TRP s explicitně zřetězenými synonymy.
** Tato funkce zjišťuje hodnotu datové části položky zadané klíčem.
** Pokud je položka nalezena, vrací funkce ukazatel na položku
** Pokud položka nalezena nebyla, vrací se funkční hodnota NULL
**
** Využijte dříve vytvořenou funkci HTSearch.
*/

tData* htRead ( tHTable* ptrht, tKey key ) {
	if ((*ptrht) == NULL){						// Testování zda ukazatel je NULL
		return (NULL);
	} else{
		tHTItem *pom = htSearch(ptrht, key);	// Vhledání položky v hashovací tabulce
		if (pom == NULL){
			return (NULL);						// Pokud nenašel
		} else {
			return (&(pom->data));				// Pokud našel tak tak vraci ukazatel na data
		}
	}
}

/*
** TRP s explicitně zřetězenými synonymy.
** Tato procedura vyjme položku s klíčem key z tabulky
** ptrht.  Uvolněnou položku korektně zrušte.  Pokud položka s uvedeným
** klíčem neexistuje, dělejte, jako kdyby se nic nestalo (tj. nedělejte
** nic).
**
** V tomto případě NEVYUŽÍVEJTE dříve vytvořenou funkci HTSearch.
*/

void htDelete ( tHTable* ptrht, tKey key ) {
	if ((*ptrht) == NULL){
		return;
	} else {
		int i = hashCode(key);			// Zahashuji si klíč
		tHTItem *pom = (*ptrht)[i];		// Získam ukazatel na nalezenou položku
		tHTItem *pom1 = NULL;
		if (pom == NULL){				// Testuji zda jsem nalezl danou pložku
			return;
		} else {					// Pokud jsem položku našel
			if (pom->key == key){ 	// Hledana polozka je první v seznamu
				pom1 = pom;			// Zalohuji si ukazatel
				(*ptrht)[i] = pom->ptrnext;	// Posunu ukazatel seznamu na další položku
				free(pom1);			// Uvolním alkovanou pamět
			} else {
				while (pom->ptrnext != NULL){
					if (pom->ptrnext->key == key){
						pom1 = pom->ptrnext;					// Zalohuji si ukazatel
						pom->ptrnext = pom->ptrnext->ptrnext;	// Posunu ukazatel seznamu na další položku
						free(pom1);								// Uvolním alkovanou pamět
					} else {
						pom = pom->ptrnext;
					}
				}
			}
		}
	}
}

/* TRP s explicitně zřetězenými synonymy.
** Tato procedura zruší všechny položky tabulky, korektně uvolní prostor,
** který tyto položky zabíraly, a uvede tabulku do počátečního stavu.
*/

void htClearAll ( tHTable* ptrht ) {
	int i = 0;
	if ((*ptrht) == NULL){					// Testování zda ukazatel je NULL
		return;
	} else {
		tHTItem *pom = NULL;
		tHTItem *pom2 = NULL;
		while (i < HTSIZE){				// Procházím hashovací tabulkou
			pom = (*ptrht)[i];			// Získám ukazatel na polozku z hashovaci tabulky
			while (pom != NULL){		// procházím a mažu další položky
				pom2 = pom;
				pom = pom->ptrnext;
				free(pom2);
			}
			(*ptrht)[i] = NULL;			// Nastavuji na NULL, aby bylo vše tak jak po inicializaci
			i++;
		}

	}
}
