
/* c201.c *********************************************************************}
{* T�ma: Jednosm�rn� line�rn� seznam
**
**                     N�vrh a referen�n� implementace: Petr P�ikryl, ��jen 1994
**                                          �pravy: Andrea N�mcov� listopad 1996
**                                                   Petr P�ikryl, listopad 1997
**                                P�epracovan� zad�n�: Petr P�ikryl, b�ezen 1998
**                                  P�epis do jazyka C: Martin Tu�ek, ��jen 2004
**	                                      �pravy: Bohuslav K�ena, ��jen 2015
**
** Implementujte abstraktn� datov� typ jednosm�rn� line�rn� seznam.
** U�ite�n�m obsahem prvku seznamu je cel� ��slo typu int.
** Seznam bude jako datov� abstrakce reprezentov�n prom�nnou typu tList.
** Definici konstant a typ� naleznete v hlavi�kov�m souboru c201.h.
**
** Va��m �kolem je implementovat n�sleduj�c� operace, kter� spolu s v��e
** uvedenou datovou ��st� abstrakce tvo�� abstraktn� datov� typ tList:
**
**      InitList ...... inicializace seznamu p�ed prvn�m pou�it�m,
**      DisposeList ... zru�en� v�ech prvk� seznamu,
**      InsertFirst ... vlo�en� prvku na za��tek seznamu,
**      First ......... nastaven� aktivity na prvn� prvek,
**      CopyFirst ..... vrac� hodnotu prvn�ho prvku,
**      DeleteFirst ... zru�� prvn� prvek seznamu,
**      PostDelete .... ru�� prvek za aktivn�m prvkem,
**      PostInsert .... vlo�� nov� prvek za aktivn� prvek seznamu,
**      Copy .......... vrac� hodnotu aktivn�ho prvku,
**      Actualize ..... p�ep��e obsah aktivn�ho prvku novou hodnotou,
**      Succ .......... posune aktivitu na dal�� prvek seznamu,
**      Active ........ zji��uje aktivitu seznamu.
**
** P�i implementaci funkc� nevolejte ��dnou z funkc� implementovan�ch v r�mci
** tohoto p��kladu, nen�-li u dan� funkce explicitn� uvedeno n�co jin�ho.
**
** Nemus�te o�et�ovat situaci, kdy m�sto leg�ln�ho ukazatele na seznam
** p�ed� n�kdo jako parametr hodnotu NULL.
**
** Svou implementaci vhodn� komentujte!
**
** Terminologick� pozn�mka: Jazyk C nepou��v� pojem procedura.
** Proto zde pou��v�me pojem funkce i pro operace, kter� by byly
** v algoritmick�m jazyce Pascalovsk�ho typu implemenov�ny jako
** procedury (v jazyce C procedur�m odpov�daj� funkce vracej�c� typ void).
**/

#include "c201.h"

int solved;
int errflg;

void Error() {
/*
** Vytiskne upozorn�n� na to, �e do�lo k chyb�.
** Tato funkce bude vol�na z n�kter�ch d�le implementovan�ch operac�.
**/
    printf ("*ERROR* The program has performed an illegal operation.\n");
    errflg = TRUE;                      /* glob�ln� prom�nn� -- p��znak chyby */
}

void InitList (tList *L) {
/*
** Provede inicializaci seznamu L p�ed jeho prvn�m pou�it�m (tzn. ��dn�
** z n�sleduj�c�ch funkc� nebude vol�na nad neinicializovan�m seznamem).
** Tato inicializace se nikdy nebude prov�d�t nad ji� inicializovan�m
** seznamem, a proto tuto mo�nost neo�et�ujte. V�dy p�edpokl�dejte,
** �e neinicializovan� prom�nn� maj� nedefinovanou hodnotu.
**/
	if (L == NULL){		//Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
		return;
	} else {
		//inicializace prom�nn� seznamu
		L->Act = NULL;
		L->First = NULL;
	}
}

void DisposeList (tList *L) {
/*
** Zru�� v�echny prvky seznamu L a uvede seznam L do stavu, v jak�m se nach�zel
** po inicializaci. Ve�ker� pam� pou��van� prvky seznamu L bude korektn�
** uvoln�na vol�n�m operace free.
***/
	tElemPtr prvek;						//pomocna promn�nn�
	if ((L == NULL) || (L->First == NULL)){		//Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
		return;
	} else {
		while ((L->First != NULL)){
			prvek = L->First;			//zkopirujeme ukazatel
			L->First = prvek->ptr;		//nastav�me ukazatel na druh� prvek v seznamu
			free(prvek);				//uvoln�n� prvku z pam�ti
		}
		L->Act = NULL;					//vypr�zd�n� seznam nem� aktivn� prvky
	}
}

void InsertFirst (tList *L, int val) {
/*
** Vlo�� prvek s hodnotou val na za��tek seznamu L.
** V p��pad�, �e nen� dostatek pam�ti pro nov� prvek p�i operaci malloc,
** vol� funkci Error().
**/
	tElemPtr prvek = (tElemPtr) malloc(sizeof(struct tElem));	//Alokace pam�ti pro nov� prvek + p�etypov�n� malloc

	if (prvek == NULL){	//Testov�n� zda alokace usp�la
		Error();		//Vol�n� chyby
	} else {
		//Vlo�en� prvku na za��tek seznamu
		prvek->data = val;
		prvek->ptr = L->First;
		L->First = prvek;
	}
}

void First (tList *L) {
/*
** Nastav� aktivitu seznamu L na jeho prvn� prvek.
** Funkci implementujte jako jedin� p��kaz, ani� byste testovali,
** zda je seznam L pr�zdn�.
**/
	L->Act = L->First; //zm�na aktivity
}

void CopyFirst (tList *L, int *val) {
/*
** Prost�ednictv�m parametru val vr�t� hodnotu prvn�ho prvku seznamu L.
** Pokud je seznam L pr�zdn�, vol� funkci Error().
**/
	if ((L == NULL)){		//Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
		return;
	} else if (L->First == NULL){ 	//Testov�n� zda seznam je pr�zny
		Error(); 					//Vol�n� chyby
		return;
	} else {
		*val = L->First->data;		//Z�sk�n� dat
	}
}

void DeleteFirst (tList *L) {
/*
** Zru�� prvn� prvek seznamu L a uvoln� j�m pou��vanou pam�.
** Pokud byl ru�en� prvek aktivn�, aktivita seznamu se ztr�c�.
** Pokud byl seznam L pr�zdn�, nic se ned�je.
**/
	tElemPtr prvek;
	// 1. Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
	// 2. Testov�n� zda seznam nen� pr�zdn�
	if ((L == NULL) || (L->First == NULL)){
		return;
	} else {
		//Pokud je prvn� prvek i aktivn� tak aktivita zanika
		if(L->First == L->Act){
			L->Act = NULL;
		}
		//odstran�n� prvn�ho prvku
		prvek = L->First;
		L->First = prvek->ptr;
		free(prvek);	//uvoln�n�
	}
}

void PostDelete (tList *L) {
/*
** Zru�� prvek seznamu L za aktivn�m prvkem a uvoln� j�m pou��vanou pam�.
** Pokud nen� seznam L aktivn� nebo pokud je aktivn� posledn� prvek seznamu L,
** nic se ned�je.
**/
	tElemPtr prvek;
	// 1. Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
	// 2. Testov�n� zda seznam nen� pr�zdn�
	// 3. Testovan� zda seznam je aktivn�
	// 4. Testov�n� zda aktivn� prvek neni posledn�
	if ((L == NULL) || (L->First == NULL) || (L->Act == NULL) || (L->Act->ptr == NULL)){
		return;
	} else {
		//odstranovan� prvku za aktivn�m prvkem
		prvek = L->Act->ptr;
		L->Act->ptr = prvek->ptr;
		free(prvek);
	}
}

void PostInsert (tList *L, int val) {
/*
** Vlo�� prvek s hodnotou val za aktivn� prvek seznamu L.
** Pokud nebyl seznam L aktivn�, nic se ned�je!
** V p��pad�, �e nen� dostatek pam�ti pro nov� prvek p�i operaci malloc,
** zavol� funkci Error().
**/
	tElemPtr prvek;		//Pomocn� prom�nna
	// 1. Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
	// 2. Testov�n� zda seznam nen� pr�zdn�
	// 3. Testovan� zda seznam je aktivn�
	if ((L == NULL) || (L->First == NULL) || (L->Act == NULL)){
			return;
		} else {
			prvek = (tElemPtr) malloc(sizeof(struct tElem)); 	//Alokace pam�ti pro nov� prvek + p�etypov�n� malloc
			if (prvek == NULL){								//Testov�n� zda alokace usp�la
				Error();									//P�i neusp�chu se vol� Error()
				return;
			} else {
				//vlo�en� prvku do seznamu za aktivn� prvek
				prvek->data = val;
				prvek->ptr = L->Act->ptr;
				L->Act->ptr = prvek;
			}
		}
}

void Copy (tList *L, int *val) {
/*
** Prost�ednictv�m parametru val vr�t� hodnotu aktivn�ho prvku seznamu L.
** Pokud seznam nen� aktivn�, zavol� funkci Error().
**/
	if ((L == NULL)){	//Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
		return;
	} else if (L->Act == NULL){	//Testovan� zda seznam m� aktivn� prvek jestli ano vrac�m jej jinak Error()
		Error();				//Volan� chybov�ho hl�en�
		return;
	} else {
		*val = L->Act->data;	//Navr�cen� hodnoty aktivn�ho prvku
	}
}

void Actualize (tList *L, int val) {
/*
** P�ep��e data aktivn�ho prvku seznamu L hodnotou val.
** Pokud seznam L nen� aktivn�, ned�l� nic!
**/
	// 1. Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
	// 2. Testov�n� zda seznam nen� pr�zdn�
	// 3. Testovan� zda seznam je aktivn�
	if ((L == NULL) || (L->First == NULL) || (L->Act == NULL)){
		return;
	} else {
		L->Act->data = val; //Nastvaven� hodnoty aktivn�ho prvku
	}
}

void Succ (tList *L) {
/*
** Posune aktivitu na n�sleduj�c� prvek seznamu L.
** V�imn�te si, �e touto operac� se m��e aktivn� seznam st�t neaktivn�m.
** Pokud nen� p�edan� seznam L aktivn�, ned�l� funkce nic.
**/
	// 1. Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
	// 2. Testov�n� zda seznam nen� pr�zdn�
	// 3. Testovan� zda seznam je aktivn�
	if ((L == NULL) || (L->First == NULL) || (L->Act == NULL)) {
		return;
	} else {
		L->Act = L->Act->ptr;	//Posunut� aktivity na n�sleduj�c� prvek
	}
}

int Active (tList *L) {
/*
** Je-li seznam L aktivn�, vrac� nenulovou hodnotu, jinak vrac� 0.
** Tuto funkci je vhodn� implementovat jedn�m p��kazem return.
**/
	// 1. Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
	// 2. Testovan� zda seznam je aktivn�
	if ((L == NULL) || (L->Act == NULL)){
		return 0;
	} else {
		return 1;
	}
}

/* Konec c201.c */
