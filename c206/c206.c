	
/* c206.c **********************************************************}
{* T�ma: Dvousm�rn� v�zan� line�rn� seznam
**
**                   N�vrh a referen�n� implementace: Bohuslav K�ena, ��jen 2001
**                            P�epracovan� do jazyka C: Martin Tu�ek, ��jen 2004
**                                            �pravy: Bohuslav K�ena, ��jen 2015
**
** Implementujte abstraktn� datov� typ dvousm�rn� v�zan� line�rn� seznam.
** U�ite�n�m obsahem prvku seznamu je hodnota typu int.
** Seznam bude jako datov� abstrakce reprezentov�n prom�nnou
** typu tDLList (DL znamen� Double-Linked a slou�� pro odli�en�
** jmen konstant, typ� a funkc� od jmen u jednosm�rn� v�zan�ho line�rn�ho
** seznamu). Definici konstant a typ� naleznete v hlavi�kov�m souboru c206.h.
**
** Va��m �kolem je implementovat n�sleduj�c� operace, kter� spolu
** s v��e uvedenou datovou ��st� abstrakce tvo�� abstraktn� datov� typ
** obousm�rn� v�zan� line�rn� seznam:
**
**      DLInitList ...... inicializace seznamu p�ed prvn�m pou�it�m,
**      DLDisposeList ... zru�en� v�ech prvk� seznamu,
**      DLInsertFirst ... vlo�en� prvku na za��tek seznamu,
**      DLInsertLast .... vlo�en� prvku na konec seznamu,
**      DLFirst ......... nastaven� aktivity na prvn� prvek,
**      DLLast .......... nastaven� aktivity na posledn� prvek,
**      DLCopyFirst ..... vrac� hodnotu prvn�ho prvku,
**      DLCopyLast ...... vrac� hodnotu posledn�ho prvku,
**      DLDeleteFirst ... zru�� prvn� prvek seznamu,
**      DLDeleteLast .... zru�� posledn� prvek seznamu,
**      DLPostDelete .... ru�� prvek za aktivn�m prvkem,
**      DLPreDelete ..... ru�� prvek p�ed aktivn�m prvkem,
**      DLPostInsert .... vlo�� nov� prvek za aktivn� prvek seznamu,
**      DLPreInsert ..... vlo�� nov� prvek p�ed aktivn� prvek seznamu,
**      DLCopy .......... vrac� hodnotu aktivn�ho prvku,
**      DLActualize ..... p�ep��e obsah aktivn�ho prvku novou hodnotou,
**      DLSucc .......... posune aktivitu na dal�� prvek seznamu,
**      DLPred .......... posune aktivitu na p�edchoz� prvek seznamu,
**      DLActive ........ zji��uje aktivitu seznamu.
**
** P�i implementaci jednotliv�ch funkc� nevolejte ��dnou z funkc�
** implementovan�ch v r�mci tohoto p��kladu, nen�-li u funkce
** explicitn� uvedeno n�co jin�ho.
**
** Nemus�te o�et�ovat situaci, kdy m�sto leg�ln�ho ukazatele na seznam
** p�ed� n�kdo jako parametr hodnotu NULL.
**
** Svou implementaci vhodn� komentujte!
**
** Terminologick� pozn�mka: Jazyk C nepou��v� pojem procedura.
** Proto zde pou��v�me pojem funkce i pro operace, kter� by byly
** v algoritmick�m jazyce Pascalovsk�ho typu implemenov�ny jako
** procedury (v jazyce C procedur�m odpov�daj� funkce vracej�c� typ void).
**/

#include "c206.h"

int solved;
int errflg;

void DLError() {
/*
** Vytiskne upozorn�n� na to, �e do�lo k chyb�.
** Tato funkce bude vol�na z n�kter�ch d�le implementovan�ch operac�.
**/
    printf ("*ERROR* The program has performed an illegal operation.\n");
    errflg = TRUE;             /* glob�ln� prom�nn� -- p��znak o�et�en� chyby */
    return;
}

void DLInitList (tDLList *L) {
/*
** Provede inicializaci seznamu L p�ed jeho prvn�m pou�it�m (tzn. ��dn�
** z n�sleduj�c�ch funkc� nebude vol�na nad neinicializovan�m seznamem).
** Tato inicializace se nikdy nebude prov�d�t nad ji� inicializovan�m
** seznamem, a proto tuto mo�nost neo�et�ujte. V�dy p�edpokl�dejte,
** �e neinicializovan� prom�nn� maj� nedefinovanou hodnotu.
**/
   if (L == NULL){		//Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
	   return;
   } else {
	   //inicializace prom�nn� seznamu
	   L->Act = NULL;
	   L->First = NULL;
	   L->Last = NULL;
   }
}

void DLDisposeList (tDLList *L) {
/*
** Zru�� v�echny prvky seznamu L a uvede seznam do stavu, v jak�m
** se nach�zel po inicializaci. Ru�en� prvky seznamu budou korektn�
** uvoln�ny vol�n�m operace free.
**/
	tDLElemPtr prvek;	//pomocna promn�nn�
	// 1. Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
	// 2. Testov�n� zda seznam nen� pr�zdn�
	if ((L == NULL) || (L->First == NULL)){
		return;
	} else {
		while ((L->First != NULL)){
			prvek = L->First;			//zkopirujeme ukazatel
			L->First = L->First->rptr;	//nastav�me ukazatel na druh� prvek v seznamu
			free(prvek);				//uvoln�n� prvku z pam�ti
		}
		L->Last = NULL;
		L->Act = NULL;					//vypr�zd�n� seznam nem� aktivn� prvky
	}
}

void DLInsertFirst (tDLList *L, int val) {
/*
** Vlo�� nov� prvek na za��tek seznamu L.
** V p��pad�, �e nen� dostatek pam�ti pro nov� prvek p�i operaci malloc,
** vol� funkci DLError().
**/
	if (L == NULL){	//Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
		return;
	}
	tDLElemPtr prvek = (tDLElemPtr) malloc(sizeof(struct tDLElem)); //Alokace pam�ti pro nov� prvek + p�etypov�n� malloc
	if (prvek == NULL){		//Testov�n� zda alokace usp�la
		DLError();			//P�i neusp�chu se vol� DLError()
		return;
	} else {
		prvek->data = val;	//Ulo�en� dat
		//Vlo�en� prvku do seznamu
		prvek->rptr = L->First;
		prvek->lptr = NULL;
		if (L->First == NULL){ //pokud se jedn� o prvn� vlo�en� tak nastav�me i ukazatel na posledn� prvek
			L->Last = prvek;
		} else {
			L->First->lptr = prvek;	//pokud nebyl seznam pr�zdn� tak p�vodn� prvn� prvek zm�n�me mu lev� ukazatel ukazatel
		}
		L->First = prvek;
	}
}

void DLInsertLast(tDLList *L, int val) {
/*
** Vlo�� nov� prvek na konec seznamu L (symetrick� operace k DLInsertFirst).
** V p��pad�, �e nen� dostatek pam�ti pro nov� prvek p�i operaci malloc,
** vol� funkci DLError().
**/
	if (L == NULL){	//Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
		return;
	}
	tDLElemPtr prvek = (tDLElemPtr) malloc(sizeof(struct tDLElem)); //Alokace pam�ti pro nov� prvek + p�etypov�n� malloc
	if (prvek == NULL){		//Testov�n� zda alokace usp�la
		DLError();			//P�i neusp�chu se vol� DLError()
		return;
	} else {
		prvek->data = val;	//Ulo�en� dat
		//Vlo�en� prvku do seznamu
		prvek->lptr = L->Last;
		prvek->rptr = NULL;
		if (L->First == NULL){ //pokud se jedn� o prvn� vlo�en� tak nastav�me i ukazatel na prvn� prvek prvek
			L->First = prvek;
		} else {
			L->Last->rptr = prvek; //pokud nebyl seznam pr�zdn� tak p�vdn� prvn� prvek zm�n�me mu prav� ukazatel
		}
		L->Last = prvek;
	}
}

void DLFirst (tDLList *L) {
/*
** Nastav� aktivitu na prvn� prvek seznamu L.
** Funkci implementujte jako jedin� p��kaz (nepo��t�me-li return),
** ani� byste testovali, zda je seznam L pr�zdn�.
**/
	L->Act = L->First;
}

void DLLast (tDLList *L) {
/*
** Nastav� aktivitu na posledn� prvek seznamu L.
** Funkci implementujte jako jedin� p��kaz (nepo��t�me-li return),
** ani� byste testovali, zda je seznam L pr�zdn�.
**/
	L->Act = L->Last;
}

void DLCopyFirst (tDLList *L, int *val) {
/*
** Prost�ednictv�m parametru val vr�t� hodnotu prvn�ho prvku seznamu L.
** Pokud je seznam L pr�zdn�, vol� funkci DLError().
**/
	if (L == NULL){	//Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
		return;
	} else if (L->First == NULL){	//pokud je seznam pr�zdy vol�me DLError()
		DLError();
		return;
	} else {
		*val = L->First->data; //Navr�cen� hodnoty prvn�ho prvku
	}
}

void DLCopyLast (tDLList *L, int *val) {
/*
** Prost�ednictv�m parametru val vr�t� hodnotu posledn�ho prvku seznamu L.
** Pokud je seznam L pr�zdn�, vol� funkci DLError().
**/
	if (L == NULL){	//Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
		return;
	} else if (L->First == NULL){	//pokud je seznam pr�zdy vol�me DLError()
		DLError();
		return;
	} else {
		*val = L->Last->data;	//Navr�cen� hodnoty posledn�ho prvku
	}
}

void DLDeleteFirst (tDLList *L) {
/*
** Zru�� prvn� prvek seznamu L. Pokud byl prvn� prvek aktivn�, aktivita
** se ztr�c�. Pokud byl seznam L pr�zdn�, nic se ned�je.
**/
	tDLElemPtr prvek;	//pomocna promn�nn�
	// 1. Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
	// 2. Testov�n� zda seznam nen� pr�zdn�
	if ((L == NULL) || (L->First == NULL)){
		return;
	} else {
		if (L->First == L->Act){	//Pokud je prvn� prvek i aktivn� tak aktivita zanika
			L->Act = NULL;
		}
		prvek = L->First;
		//odstran�n� prvku ze seznamu
		if (L->First->rptr == NULL){//obsahuje pouze jeden prvek
			L->First = NULL;
			L->Last = NULL;
		} else {					//obsahuje vice prvku
			L->First = L->First->rptr;
			L->First->lptr = NULL;
		}
		free(prvek);	//uvoln�n� prvku z pam�ti

	}
}	

void DLDeleteLast (tDLList *L) {
/*
** Zru�� posledn� prvek seznamu L. Pokud byl posledn� prvek aktivn�,
** aktivita seznamu se ztr�c�. Pokud byl seznam L pr�zdn�, nic se ned�je.
**/
	tDLElemPtr prvek;	//pomocna promn�nn�
	// 1. Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
	// 2. Testov�n� zda seznam nen� pr�zdn�
	if ((L == NULL) || (L->First == NULL)){
		return;
	} else {
		if (L->Last == L->Act){ //Pokud je posledn� prvek i aktivn� tak aktivita zanika
			L->Act = NULL;
		}
		prvek = L->Last;
		//odstran�n� prvku ze seznamu
		if (L->Last->lptr == NULL){	//obsahuje pouze jeden prvek
			L->First = NULL;
			L->Last = NULL;
		} else {					//obsahuje vice prvku
			L->Last = L->Last->lptr;
			L->Last->rptr = NULL;
		}
		free(prvek);	//uvoln�n� prvku z pam�ti
	}
}

void DLPostDelete (tDLList *L) {
/*
** Zru�� prvek seznamu L za aktivn�m prvkem.
** Pokud je seznam L neaktivn� nebo pokud je aktivn� prvek
** posledn�m prvkem seznamu, nic se ned�je.
**/
	tDLElemPtr prvek;
	// 1. Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
	// 2. Testov�n� zda seznam nen� pr�zdn�
	// 3. Testovan� zda seznam je aktivn�
	// 4. Testov�n� zda aktivn� prvek neni posledn�
	if ((L == NULL) || (L->First == NULL) || (L->Act == NULL) || (L->Act->rptr == NULL)){
		return;
	} else {
		//Odrstarnovan� prvku ze seznamu
		prvek = L->Act->rptr;
		//odstran�n� prvku ze seznamu
		L->Act->rptr = prvek->rptr;
		if (prvek->rptr == NULL){	//Pokud je mazan� prvek posledn�m
			L->Last = L->Act;
		} else {
			prvek->rptr->lptr = L->Act;
		}
		free(prvek);	//uvoln�n� prvku z pam�ti
	}
}

void DLPreDelete (tDLList *L) {
/*
** Zru�� prvek p�ed aktivn�m prvkem seznamu L .
** Pokud je seznam L neaktivn� nebo pokud je aktivn� prvek
** prvn�m prvkem seznamu, nic se ned�je.
**/
	tDLElemPtr prvek;
	// 1. Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
	// 2. Testov�n� zda seznam nen� pr�zdn�
	// 3. Testovan� zda seznam je aktivn�
	// 4. Testov�n� zda aktivn� prvek neni prvn�
	if ((L == NULL) || (L->First == NULL) || (L->Act == NULL) || (L->Act->lptr == NULL)){
		return;
	} else {
		// Vlo�en� prvku do seznamu
		prvek = L->Act->lptr;
		//odstran�n� prvku ze seznamu
		L->Act->lptr = prvek->lptr;
		if (prvek->lptr == NULL){	//Pokud je mazan� prvek prvn�m
			L->First = L->Act;
		} else {
			prvek->lptr->rptr = L->Act;
		}
		free(prvek);	//uvoln�n� prvku z pam�ti
	}
}

void DLPostInsert (tDLList *L, int val) {
/*
** Vlo�� prvek p�ed aktivn� prvek seznamu L.
** Pokud nebyl seznam L aktivn�, nic se ned�je.
** V p��pad�, �e nen� dostatek pam�ti pro nov� prvek p�i operaci malloc,
** vol� funkci DLError().
**/
	// 1. Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
	// 2. Testov�n� zda seznam nen� pr�zdn�
	// 3. Testovan� zda seznam je aktivn�
	if ((L == NULL) || (L->First == NULL) || (L->Act == NULL)){
		return;
	} else {
		tDLElemPtr prvek = (tDLElemPtr) malloc(sizeof(struct tDLElem));	//Alokace pam�ti pro nov� prvek + p�etypov�n� malloc
		if (prvek == NULL){ //Testov�n� zda alokace usp�la
			DLError();		//P�i neusp�chu se vol� DLError()
			return;
		}
		prvek->data = val;	//Ulo�en� dat
		prvek->lptr = L->Act;
		//vlo�en� prvku do seznamu za aktivn� prvek
		if (L->Act->rptr == NULL){ //pokud je aktivn� prvek posledn�m
			L->Last = prvek;
			prvek->rptr = NULL;
		} else {
			L->Act->rptr->lptr = prvek;
			prvek->rptr = L->Act->rptr;
		}
		L->Act->rptr = prvek;
	}
}

void DLPreInsert (tDLList *L, int val) {
/*
** Vlo�� prvek p�ed aktivn� prvek seznamu L.
** Pokud nebyl seznam L aktivn�, nic se ned�je.
** V p��pad�, �e nen� dostatek pam�ti pro nov� prvek p�i operaci malloc,
** vol� funkci DLError().
**/
	// 1. Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
	// 2. Testov�n� zda seznam nen� pr�zdn�
	// 3. Testovan� zda seznam je aktivn�
	if ((L == NULL) || (L->First == NULL) || (L->Act == NULL)){
		return;
	} else {
		tDLElemPtr prvek = (tDLElemPtr) malloc(sizeof(struct tDLElem));	//Alokace pam�ti pro nov� prvek + p�etypov�n� malloc
		if (prvek == NULL){	//Testov�n� zda alokace usp�la
			DLError();		//P�i neusp�chu se vol� DLError()
			return;
		}
		prvek->data = val;	//Ulo�en� dat
		//vlo�en� prvku do seznamu p�ed aktivn� prvek
		prvek->rptr = L->Act;
		if (L->Act->lptr == NULL){	//pokud je aktivn� prvek prvn�m
			L->First = prvek;
			prvek->lptr = NULL;
		} else {
			L->Act->lptr->rptr = prvek;
			prvek->lptr = L->Act->lptr;
		}
		L->Act->lptr = prvek;
	}
}

void DLCopy (tDLList *L, int *val) {
/*
** Prost�ednictv�m parametru val vr�t� hodnotu aktivn�ho prvku seznamu L.
** Pokud seznam L nen� aktivn�, vol� funkci DLError ().
**/
	if (L == NULL){	//Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
		return;
	} else if (L->Act == NULL){
		DLError();
	} else {
		*val = L->Act->data;	//Navr�cen� hodnoty aktivn�ho prvku
	}
}

void DLActualize (tDLList *L, int val) {
/*
** P�ep��e obsah aktivn�ho prvku seznamu L.
** Pokud seznam L nen� aktivn�, ned�l� nic.
**/
	// 1. Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
	// 2. Testov�n� zda seznam nen� pr�zdn�
	// 3. Testovan� zda seznam je aktivn�
	if ((L == NULL) || (L->First == NULL) || (L->Act == NULL)){
		return;
	} else {
		L->Act->data = val; //Aktualizace hodnoty aktivn�ho prvku
	}
}

void DLSucc (tDLList *L) {
/*
** Posune aktivitu na n�sleduj�c� prvek seznamu L.
** Nen�-li seznam aktivn�, ned�l� nic.
** V�imn�te si, �e p�i aktivit� na posledn�m prvku se seznam stane neaktivn�m.
**/
	// 1. Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
	// 2. Testov�n� zda seznam nen� pr�zdn�
	// 3. Testovan� zda seznam je aktivn�
	if ((L == NULL) || (L->First == NULL) || (L->Act == NULL)){
		return;
	} else {
		L->Act = L->Act->rptr;	//posunut� aktivity
	}
}

void DLPred (tDLList *L) {
/*
** Posune aktivitu na p�edchoz� prvek seznamu L.
** Nen�-li seznam aktivn�, ned�l� nic.
** V�imn�te si, �e p�i aktivit� na prvn�m prvku se seznam stane neaktivn�m.
**/
	// 1. Testov�n�, zda ukazatel nen� NULL (nen� povinn�)
	// 2. Testov�n� zda seznam nen� pr�zdn�
	// 3. Testovan� zda seznam je aktivn�
	if ((L == NULL) || (L->First == NULL) || (L->Act == NULL)){
		return;
	} else {
		L->Act = L->Act->lptr; //posunut� aktivity
	}
}

int DLActive (tDLList *L) {
/*
** Je-li seznam L aktivn�, vrac� nenulovou hodnotu, jinak vrac� 0.
** Funkci je vhodn� implementovat jedn�m p��kazem return.
**/
	return (L->Act != NULL)? 1 : 0;
}

/* Konec c206.c*/
